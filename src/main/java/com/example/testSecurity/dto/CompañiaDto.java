package com.example.testSecurity.dto;

import lombok.Data;
@Data
public class CompañiaDto {
	private Long id;
	private int cif;
	private String nombre;
}
