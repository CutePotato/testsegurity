package com.example.testSecurity.dto;

import lombok.Data;
@Data
public class TiendaDto {
	private Long id;
	private String nombre;
	private String direccion;
}
