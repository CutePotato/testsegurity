package com.example.testSecurity.dto;

import com.example.testSecurity.enums.RolCliente;

import lombok.Data;

@Data
public class RolDto {
	private Long id;
	private RolCliente rol;
}
