package com.example.testSecurity.dto;

import com.example.testSecurity.enums.EstadoStock;

import lombok.Data;

@Data
public class StockDto {
	private Long id;
	private EstadoStock estado;
	private int reference;
	
	@Override
	public String toString() {
		return "Estado: "+this.estado.toString()+", NºReferencia"+this.reference;
	}
}
