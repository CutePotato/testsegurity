package com.example.testSecurity.dto;

import java.util.Date;
import java.util.List;

//import javax.validation.constraints.NotBlank;

import com.example.testSecurity.enums.Categoria;

import lombok.Data;
@Data
public class JuegoDto {
	private Long id;
	//@NotBlank()
	private String titulo;
	//@JsonFormat(pattern = "yyyy-mm-dd", locale = "ES")
	private Date fechaLanzamiento;
	private Categoria categoria;
	private int pegi;
	private List<CompañiaDto>companias;
}
