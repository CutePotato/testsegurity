package com.example.testSecurity.dto;

import java.util.Date;

import lombok.Data;
@Data
public class ClienteDto {
	private Long id;
	private String nombre;
	private Date fechaNacimiento;
	private String correo;
	private String documento;
	private String username;
	private String password;
}