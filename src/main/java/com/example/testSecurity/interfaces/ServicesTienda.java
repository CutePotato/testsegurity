package com.example.testSecurity.interfaces;

import com.example.testSecurity.dto.TiendaDto;

public interface ServicesTienda {
	public TiendaDto createTienda(TiendaDto tienda);
	public TiendaDto updateTienda(Long id, TiendaDto tienda);
	public TiendaDto deleteTienda(Long id);
}
