package com.example.testSecurity.interfaces;

import com.example.testSecurity.dto.StockDto;

public interface ServicesStock {
	public StockDto createStock(StockDto stock);
	public StockDto updateStock(Long id, StockDto stock);
	public StockDto deleteStock(Long id);
}
