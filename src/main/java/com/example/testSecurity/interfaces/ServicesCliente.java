package com.example.testSecurity.interfaces;

import com.example.testSecurity.dto.ClienteDto;

public interface ServicesCliente {

	public ClienteDto createClient(ClienteDto cliente);

	public ClienteDto updateClient(Long id, ClienteDto cliente);

	public ClienteDto deleteClient(Long id);
}
