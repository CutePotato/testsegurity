package com.example.testSecurity.interfaces;

import com.example.testSecurity.dto.CompañiaDto;

public interface ServicesCompañia {
	public CompañiaDto createCompañia(CompañiaDto comp);
	public CompañiaDto updateCompañia(Long id, CompañiaDto comp);
	public CompañiaDto deleteCompañia(Long id);
}
