package com.example.testSecurity.interfaces;

import com.example.testSecurity.dto.JuegoDto;

public interface ServicesJuego {
	public JuegoDto createJuego(JuegoDto juego);
	public JuegoDto updateJuego(Long id, JuegoDto juego);
	public JuegoDto deleteJuego(Long id);
}
