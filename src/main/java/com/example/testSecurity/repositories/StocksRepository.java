package com.example.testSecurity.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.testSecurity.entities.Stock;
@Repository
@Transactional
public interface StocksRepository extends JpaRepository<Stock, Long>{
	public Stock findByReference(int reference); 
}
