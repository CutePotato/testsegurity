package com.example.testSecurity.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.testSecurity.entities.Cliente;
@Repository
@Transactional
public interface ClientesRepository extends JpaRepository<Cliente, Long>{
	//@Query("SELECT * FROM Cliente c where c.documento = :documento")
	//public Cliente buscarClientDocument(@Param("documento") String documento);
	public Cliente findByUsername(String username);
	
}
