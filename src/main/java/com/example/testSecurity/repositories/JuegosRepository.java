package com.example.testSecurity.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.testSecurity.entities.Juego;
@Repository
@Transactional
public interface JuegosRepository extends JpaRepository<Juego, Long>{
	public Juego findByTitulo(String titulo);
}
