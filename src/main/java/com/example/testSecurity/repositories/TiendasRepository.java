package com.example.testSecurity.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.testSecurity.entities.Tienda;
@Repository
@Transactional
public interface TiendasRepository extends JpaRepository<Tienda, Long>{
	public Tienda findByNombre(String nombre);
}
