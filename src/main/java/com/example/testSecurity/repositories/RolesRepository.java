package com.example.testSecurity.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.testSecurity.entities.Rol;
import com.example.testSecurity.enums.RolCliente;
@Repository
@Transactional
public interface RolesRepository extends JpaRepository<Rol, Long>{
	public Rol findByRol(RolCliente rol);
}
