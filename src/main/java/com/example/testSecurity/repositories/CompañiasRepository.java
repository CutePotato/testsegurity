package com.example.testSecurity.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.testSecurity.entities.Compañia;
@Repository
@Transactional
public interface CompañiasRepository extends JpaRepository<Compañia, Long>{
	public Compañia findByCif(int cif);
}
