package com.example.testSecurity.enums;

public enum EstadoStock {
	ALQUILADO, VENDIDO, DISPONIBLE
}
