package com.example.testSecurity.errors.generic;

public class MyServiceUnavailableException extends RuntimeException{
	private final static String SERVICE_UNAVAILABLE = "Servicio no disponible (503). ";
	public MyServiceUnavailableException(String message) {
		super(SERVICE_UNAVAILABLE+message);
	}
}
