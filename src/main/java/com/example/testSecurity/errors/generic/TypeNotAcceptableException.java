package com.example.testSecurity.errors.generic;

public class TypeNotAcceptableException extends RuntimeException{
	private final static String NOT_ACCEPTABLE = "No se acepta el typo (406). ";
	public TypeNotAcceptableException(String message) {
		super(NOT_ACCEPTABLE+message);
	}
}
