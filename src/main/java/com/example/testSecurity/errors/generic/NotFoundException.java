package com.example.testSecurity.errors.generic;

public class NotFoundException extends RuntimeException{
	private static final String DESCRIPTION = "Not Found Exception (404)";
	
	public NotFoundException(String details) {
		super(DESCRIPTION + ". " + details);
	}
}
