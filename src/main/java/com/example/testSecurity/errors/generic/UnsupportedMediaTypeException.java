package com.example.testSecurity.errors.generic;

public class UnsupportedMediaTypeException extends RuntimeException{
	private static final String UNSUPPORTED_MEDIA_TYPE = "Tipe media unsuported (415). ";
	public UnsupportedMediaTypeException(String message) {
		super(UNSUPPORTED_MEDIA_TYPE+message);
	}
}
