package com.example.testSecurity.errors.generic;

public class MethodNotAllowedException extends RuntimeException{
	private final static String METHOD_NOT_ALLOWED = "Metodo no permitido (405). ";
	public MethodNotAllowedException(String message) {
		super(METHOD_NOT_ALLOWED+message);
	}
}
