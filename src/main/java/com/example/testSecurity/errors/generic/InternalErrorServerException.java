package com.example.testSecurity.errors.generic;

public class InternalErrorServerException extends RuntimeException{
	public final static String SERVER_ERROR = "Internal Error Server (500). ";
	public InternalErrorServerException(String message) {
		super(SERVER_ERROR+message);
	}
}
