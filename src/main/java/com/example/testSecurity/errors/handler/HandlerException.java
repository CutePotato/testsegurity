package com.example.testSecurity.errors.handler;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.util.CollectionUtils;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.testSecurity.errors.ResponseError;
import com.example.testSecurity.errors.generic.BadRequestException;
import com.example.testSecurity.errors.generic.InternalErrorServerException;
import com.example.testSecurity.errors.generic.MethodNotAllowedException;
import com.example.testSecurity.errors.generic.MyServiceUnavailableException;
import com.example.testSecurity.errors.generic.NoContentException;
import com.example.testSecurity.errors.generic.TypeNotAcceptableException;
import com.example.testSecurity.errors.generic.UnauthorizedException;
import com.example.testSecurity.errors.generic.UnsupportedMediaTypeException;
import com.example.testSecurity.errors.operations.ClientException;
import com.example.testSecurity.errors.operations.CompañiaException;
import com.example.testSecurity.errors.operations.JuegoException;
import com.example.testSecurity.errors.operations.RolException;
import com.example.testSecurity.errors.operations.StockException;
import com.example.testSecurity.errors.operations.TiendaException;
import com.example.testSecurity.errors.operations.UsuarioException;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class HandlerException extends ResponseEntityExceptionHandler{
	
	private Logger logger = LogManager.getLogger(this);

	@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
	@ExceptionHandler({
		MethodNotAllowedException.class
	})
	@ResponseBody
	public ResponseError methodNotAllowedException(HttpServletRequest request, Exception exception) {
		
		logger.info(exception.getMessage());
		
		return new ResponseError(exception, request.getRequestURI());
	}
	
	@ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
	@ExceptionHandler({
		UnsupportedMediaTypeException.class
	})
	@ResponseBody
	public ResponseError unsupportedMediaType(HttpServletRequest request, Exception exception) {
		
		logger.info(exception.getMessage());
		
		return new ResponseError(exception, request.getRequestURI());
	}
	
	@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
	@ExceptionHandler({
		TypeNotAcceptableException.class
	})
	@ResponseBody
	public ResponseError typeNotAcceptableException(HttpServletRequest request, Exception exception) {
		return new ResponseError(exception, request.getRequestURI());
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler({
		InternalErrorServerException.class
	})
	@ResponseBody
	public ResponseError internalErrorServerException(HttpServletRequest request, Exception exception) {
		
		logger.info(exception.getMessage());
		
		return new ResponseError(exception, request.getRequestURI());
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler({
		BadRequestException.class
	})
	@ResponseBody
	public ResponseError badRequestException(HttpServletRequest request, Exception exception) {
		
		logger.info(exception.getMessage());
		
		return new ResponseError(exception, request.getRequestURI());
	}
	
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler({
		NotFoundException.class,
		NullPointerException.class
	})
	@ResponseBody
	public ResponseError notFountException(HttpServletRequest request, Exception exception) {
		
		logger.info(exception.getMessage());
		
		return new ResponseError(exception, request.getRequestURI());
	}
	
	@ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
	@ExceptionHandler({
		MyServiceUnavailableException.class
	})
	@ResponseBody
	public ResponseError myServiceUnavailableException(HttpServletRequest request, Exception exception) {
		
		logger.info(exception.getMessage());
		
		return new ResponseError(exception, request.getRequestURI());
	}
	
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ExceptionHandler({
		NoContentException.class,
		ClientException.class,
		CompañiaException.class,
		JuegoException.class,
		RolException.class,
		StockException.class,
		TiendaException.class,
		UsuarioException.class
	})
	@ResponseBody
	public ResponseError noContentException(HttpServletRequest request, Exception exception) {
		
		logger.info(exception.getMessage());
		
		return new ResponseError(exception, request.getRequestURI());
	}
	
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	@ExceptionHandler({
		UnauthorizedException.class,
		AccessDeniedException.class,
	})
	@ResponseBody
	public ResponseError unauthorizedException(HttpServletRequest request, Exception exception) {
		
		logger.info(exception.getMessage());
		
		return new ResponseError(exception, request.getRequestURI());
	}
	
	//METHOD_NOT_ALLOWED
	@Override
	protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
			HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

		pageNotFoundLogger.warn(ex.getMessage());

		Set<HttpMethod> supportedMethods = ex.getSupportedHttpMethods();
		if (!CollectionUtils.isEmpty(supportedMethods)) {
			headers.setAllow(supportedMethods);
		}
		return new ResponseEntity<Object>(new ResponseError(ex, request.getContextPath()), HttpStatus.METHOD_NOT_ALLOWED);
	}
	//UNSUPPORTED_MEDIA_TYPE
	@Override
	protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(
			HttpMediaTypeNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

		List<MediaType> mediaTypes = ex.getSupportedMediaTypes();
		if (!CollectionUtils.isEmpty(mediaTypes)) {
			headers.setAccept(mediaTypes);
		}

		return new ResponseEntity<Object>(new ResponseError(ex, request.getContextPath()), HttpStatus.UNSUPPORTED_MEDIA_TYPE);
	}
	//NOT_ACCEPTABLE
	@Override
	protected ResponseEntity<Object> handleHttpMediaTypeNotAcceptable(
			HttpMediaTypeNotAcceptableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

		return new ResponseEntity<Object>(new ResponseError(ex, request.getContextPath()), HttpStatus.NOT_ACCEPTABLE);
	}
	
	//INTERNAL_SERVER_ERROR
	@Override
	protected ResponseEntity<Object> handleMissingPathVariable(
			MissingPathVariableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

		return new ResponseEntity<Object>(new ResponseError(ex, request.getContextPath()), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	//BAD_REQUEST
	@Override
	protected ResponseEntity<Object> handleMissingServletRequestParameter(
			MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

		return new ResponseEntity<Object>(new ResponseError(ex, request.getContextPath()), HttpStatus.BAD_REQUEST);
	}
	
	//NOT_FOUND
	@Override
	protected ResponseEntity<Object> handleNoHandlerFoundException(
			NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

		return new ResponseEntity<Object>(new ResponseError(ex, request.getContextPath()), HttpStatus.NOT_FOUND);
	}
	
	//SERVICE_UNAVAILABLE
	@Override
	@Nullable
	protected ResponseEntity<Object> handleAsyncRequestTimeoutException(
			AsyncRequestTimeoutException ex, HttpHeaders headers, HttpStatus status, WebRequest webRequest) {

		if (webRequest instanceof ServletWebRequest) {
			ServletWebRequest servletWebRequest = (ServletWebRequest) webRequest;
			HttpServletResponse response = servletWebRequest.getResponse();
			if (response != null && response.isCommitted()) {
				if (logger.isWarnEnabled()) {
					logger.warn("Async request timed out");
				}
				return null;
			}
		}

		return new ResponseEntity<Object>(new ResponseError(ex, webRequest.getContextPath()), HttpStatus.SERVICE_UNAVAILABLE);
	}
	//NO_CONTENT
	@Override
	protected ResponseEntity<Object>handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request){
		List<String>errorMessages = ex.getBindingResult().getFieldErrors().stream().map(e->e.getDefaultMessage()).collect(Collectors.toList());
		return new ResponseEntity<>(new ResponseError(ex, errorMessages.toString()), HttpStatus.NO_CONTENT);
	}
	

}
