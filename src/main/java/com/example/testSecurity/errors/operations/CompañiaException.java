package com.example.testSecurity.errors.operations;

public class CompañiaException extends RuntimeException{
	private final static String COMPAÑIA_ERROR = "Error en operacion de la compañia: ";
	public CompañiaException(String message) {
		super(COMPAÑIA_ERROR+message);
	}
}