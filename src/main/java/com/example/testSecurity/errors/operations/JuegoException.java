package com.example.testSecurity.errors.operations;

public class JuegoException extends RuntimeException{
	private final static String JUEGO_ERROR = "Error en operacion del juego: ";
	public JuegoException(String message) {
		super(JUEGO_ERROR+message);
	}
}