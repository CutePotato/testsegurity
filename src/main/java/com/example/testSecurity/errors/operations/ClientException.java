package com.example.testSecurity.errors.operations;

public class ClientException extends RuntimeException{
	private final static String CLIENTE_ERROR = "Error en operacion del cliente: ";
	public ClientException(String message) {
		super(CLIENTE_ERROR+message);
	}
}
