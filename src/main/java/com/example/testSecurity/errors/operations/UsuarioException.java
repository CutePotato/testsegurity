package com.example.testSecurity.errors.operations;

public class UsuarioException extends RuntimeException{
	private final static String USUARIO_ERROR = "Error en operacion del usuario: ";
	public UsuarioException(String message) {
		super(USUARIO_ERROR+message);
	}
}