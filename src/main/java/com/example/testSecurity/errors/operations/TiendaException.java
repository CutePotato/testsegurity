package com.example.testSecurity.errors.operations;

public class TiendaException extends RuntimeException{
	private final static String TIENDA_ERROR = "Error en operacion de la tienda: ";
	public TiendaException(String message) {
		super(TIENDA_ERROR+message);
	}
}