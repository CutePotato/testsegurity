package com.example.testSecurity.errors.operations;

public class RolException extends RuntimeException{
	private final static String ROL_ERROR = "Error en operacion de rol: ";
	public RolException(String message) {
		super(ROL_ERROR+message);
	}
}