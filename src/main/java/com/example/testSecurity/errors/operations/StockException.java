package com.example.testSecurity.errors.operations;

public class StockException extends RuntimeException{
	private final static String STOCK_ERROR = "Error en operacion de stock: ";
	public StockException(String message) {
		super(STOCK_ERROR+message);
	}
}