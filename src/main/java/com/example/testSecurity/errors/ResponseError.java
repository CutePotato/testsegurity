package com.example.testSecurity.errors;

import lombok.Data;

@Data
public class ResponseError {
	private String exception;
	private String message;
	private String path;
	
	public ResponseError(Exception exception, String path) {
		this.exception = exception.getClass().getSimpleName();
		this.message = exception.getMessage();
		this.path = path;
	}
	
	public ResponseError(Exception exception, String message, String path) {
		this.exception = exception.getClass().getSimpleName();
		this.message = message;
		this.path = path;
	}
}
