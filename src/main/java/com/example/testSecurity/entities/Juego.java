package com.example.testSecurity.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.example.testSecurity.enums.Categoria;

import lombok.Data;
@Entity
@Table(name="JUEGO")
@Data
public class Juego {
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Long id;
	@Column(name="TITULO", unique=true)
	private String titulo;
	@Column(name="FECHALANZAMIENTO")
	private Date fechaLanzamiento;
	@Column(name="CATEGORIA")
	private Categoria categoria;
	@Column(name="PEGI")
	private int pegi;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "juego")
	private List<Stock>listaStock = new ArrayList<>();
	@ManyToMany(cascade = CascadeType.ALL, mappedBy = "listaJuego")
	private List<Compañia>listaCompañia = new ArrayList<>();
	@Override
	public String toString() {
		return "Juego [id=" + id + ", titulo=" + titulo + ", fechaLanzamiento=" + fechaLanzamiento + ", categoria="
				+ categoria + ", pegi=" + pegi + "]";
	}
	
}
