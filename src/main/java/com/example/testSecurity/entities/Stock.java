package com.example.testSecurity.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.example.testSecurity.enums.EstadoStock;

import lombok.Data;
@Entity
@Table(name="STOCK")
@Data
public class Stock {
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Long id;
	@Column(name="ESTADO")
	private EstadoStock estado;
	@Column(name="REFERENCE", unique=true)
	private int reference;
	@ManyToOne
	private Cliente cliente;
	@ManyToOne
	private Juego juego;
	@ManyToOne
	private Tienda tienda;
	@Override
	public String toString() {
		return "Stock [id=" + id + ", estado=" + estado + ", reference=" + reference + "]";
	}
}
