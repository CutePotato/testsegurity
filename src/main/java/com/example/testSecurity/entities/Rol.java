package com.example.testSecurity.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

import com.example.testSecurity.enums.RolCliente;

import lombok.Data;
/*Los Roles creados tienen que estar definidos previamente en El enum de RolClient*/
@SuppressWarnings("serial")
@Data
@Entity
@Table(name="ROL")
public class Rol implements GrantedAuthority{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name="ROL", unique = true)
	private RolCliente rol;
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	private List<Cliente>clientes = new ArrayList<>();
	@Override
	public String getAuthority() {
		return getRol().toString();
	}
	@Override
	public String toString() {
		return this.getRol().toString();
	}
}
