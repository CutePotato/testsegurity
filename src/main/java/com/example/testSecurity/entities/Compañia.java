package com.example.testSecurity.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="COMPANIA")
@Data
public class Compañia {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name="CIF", unique=true)
	private int cif;
	@Column(name="NOMBRE")
	private String nombre;
	@ManyToMany(cascade = CascadeType.PERSIST)
	private List<Juego>listaJuego = new ArrayList<>();
	@Override
	public String toString() {
		return "Compañia [id=" + id + ", cif=" + cif + ", nombre=" + nombre + "]";
	}
}
