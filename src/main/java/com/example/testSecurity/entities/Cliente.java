package com.example.testSecurity.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

@Entity
@Table(name="CLIENTE")
@Data
public class Cliente{
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Long id;
	@Column(name = "NAME")
	@Size(min=2, max=14)
	private String nombre;
	@Column(name = "FECHANACIMIENTO")
	private Date fechaNacimiento;
	@Column(name = "CORREO", unique=true)
	@Email
	private String correo;
	@Column(name = "DOCUMENTO", unique=true)
	@NotNull
	@Size(min=3, max=12)
	private String documento;
	private String username;
	private String password;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "cliente")
	private List<Stock>listaStock = new ArrayList<>();
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "clientes")
	private List<Rol>roles = new ArrayList<>();
	@Override
	public String toString() {
		return "Cliente [id=" + id + ", nombre=" + nombre + ", fechaNacimiento=" + fechaNacimiento + ", correo="
				+ correo + ", documento=" + documento + ", username=" + username + ", password=" + password + "]";
	}
	
}