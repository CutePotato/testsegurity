package com.example.testSecurity.services;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.testSecurity.dto.CompañiaDto;
import com.example.testSecurity.dto.JuegoDto;
import com.example.testSecurity.dto.StockDto;
import com.example.testSecurity.entities.Compañia;
import com.example.testSecurity.entities.Juego;
import com.example.testSecurity.entities.Stock;
import com.example.testSecurity.interfaces.ServicesJuego;
import com.example.testSecurity.repositories.CompañiasRepository;
import com.example.testSecurity.repositories.JuegosRepository;
import com.example.testSecurity.repositories.StocksRepository;
import com.example.testSecurity.services.converters.JuegoDtoJuegoConverter;
import com.example.testSecurity.services.converters.ListCompañiaListStringConverter;
import com.example.testSecurity.services.converters.ListStockListStringConverter;
@Service
public class ServiceJuego implements ServicesJuego{
	@Autowired
	ServiceCompañia serviceComp;
	@Autowired
	JuegosRepository juegoRepository;
	@Autowired
	CompañiasRepository compañiaRepository;
	@Autowired
	StocksRepository stockRepository;
	@Autowired
	JuegoDtoJuegoConverter converter;
	@Autowired
	ListStockListStringConverter stockConverter;
	@Autowired
	ListCompañiaListStringConverter compañiaConverter;

	@Override
	public JuegoDto createJuego(JuegoDto juego) {
		Juego j = converter.convert(juego);
		Juego game = juegoRepository.save(j);
		return converter.inverseConvert(game);
	}
	
	public List<JuegoDto>readAllJuego() {
		return converter.inverseConvertList(juegoRepository.findAll());
	}
	
	@Override
	public JuegoDto updateJuego(Long id, JuegoDto juego) {
		Juego j = juegoRepository.findById(id).get();
		try {
			j.setTitulo(juego.getTitulo());
			j.setCategoria(juego.getCategoria());
			j.setFechaLanzamiento(juego.getFechaLanzamiento());
			j.setPegi(juego.getPegi());
			Juego game = juegoRepository.save(j);
			return converter.inverseConvert(game);
		}
		catch(Exception e) {
			Logger log = LogManager.getLogger();
			log.info(e.getMessage());
		}
		return juego;
	}

	@Override
	public JuegoDto deleteJuego(Long id) {
		Juego j = juegoRepository.findById(id).get();
		JuegoDto salida = converter.inverseConvert(j);
		if(j!=null) {
			juegoRepository.delete(j);
			return salida;
		}
		return salida;
	}

	public void addStock(String titulo, StockDto stock) {
		Stock s = stockRepository.findByReference(stock.getReference());
		Juego juego = juegoRepository.findByTitulo(titulo);
		s.setJuego(juego);
		stockRepository.save(s);
		juego.getListaStock().add(s);
		juegoRepository.save(juego);
	}
	
	public List<String>readAllStocks(String titulo){
		Juego juego = juegoRepository.findByTitulo(titulo);
		List<Stock>listaStock = juego.getListaStock();
		return stockConverter.convert(listaStock);
	}
	
	public void updateStock(String titulo, StockDto stock) {
		Juego juego = juegoRepository.findByTitulo(titulo);
		Stock s = stockRepository.findByReference(stock.getReference());
		if(juego.getListaStock().contains(s)) {
			s.setEstado(stock.getEstado());
			stockRepository.save(s);
		}
	}
	
	public void deleteStock(String titulo, StockDto stock) {
		Juego juego = juegoRepository.findByTitulo(titulo);
		Stock s = stockRepository.findByReference(stock.getReference());
		if(juego.getListaStock().contains(s)) {
			juego.getListaStock().remove(s);
			s.setJuego(null);
			stockRepository.save(s);
			juegoRepository.save(juego);
		}
	}
	
	public void addCompañia(String titulo, CompañiaDto compañia) {
		Juego juego = juegoRepository.findByTitulo(titulo);
		Compañia c = compañiaRepository.findByCif(compañia.getCif());
		if(!companiaAlreadyExists(juego, c)) {
			juego.getListaCompañia().add(c);
			juegoRepository.save(juego);
		}
	}
	
	private Boolean companiaAlreadyExists(Juego juego, Compañia comp) {
		for(Compañia c : juego.getListaCompañia()) {
			if(c.getId() == comp.getId()) {
				return true;
			}
		}
		return false;
	}
	
	public List<String>readAllCompañias(String titulo){
		Juego juego = juegoRepository.findByTitulo(titulo);
		List<Compañia>listaCompañia = juego.getListaCompañia();
		return compañiaConverter.convert(listaCompañia);
	}
	
	public void updateCompañia(String titulo, CompañiaDto compañia) {
		Compañia c = compañiaRepository.findByCif(compañia.getCif());
		Juego juego = juegoRepository.findByTitulo(titulo);
		if(juego.getListaCompañia().contains(c)) {
			c.setNombre(compañia.getNombre());
			compañiaRepository.save(c);
			juegoRepository.save(juego);
		}
	}
	
	public void deleteCompañia(String titulo, CompañiaDto compañia) {
		Compañia c = compañiaRepository.findByCif(compañia.getCif());
		Juego juego = juegoRepository.findByTitulo(titulo);
		if(juego.getListaCompañia().contains(c) && c.getListaJuego().contains(juego)) {
			c.getListaJuego().remove(juego);
			juego.getListaCompañia().remove(c);
			compañiaRepository.save(c);
			juegoRepository.save(juego);
		}
	}
}
