package com.example.testSecurity.services.converters;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import com.example.testSecurity.entities.Juego;
@Service
public class ListJuegoListStringConverter implements Converter<List<Juego>, List<String>> {

	@Override
	public List<String> convert(List<Juego> source) {
		List<String>salida = new ArrayList<>();
		for (Juego juego : source) {
			salida.add(juego.toString());
		}
		return salida;
	}

}
