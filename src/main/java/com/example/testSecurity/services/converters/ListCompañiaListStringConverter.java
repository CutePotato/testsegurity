package com.example.testSecurity.services.converters;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import com.example.testSecurity.entities.Compañia;
@Service
public class ListCompañiaListStringConverter implements Converter<List<Compañia>, List<String>> {

	@Override
	public List<String> convert(List<Compañia> source) {
		List<String>salida = new ArrayList<>();
		for (Compañia compañia : source) {
			salida.add(compañia.toString());
		}
		return salida;
	}

}
