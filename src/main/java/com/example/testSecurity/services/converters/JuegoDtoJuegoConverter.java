package com.example.testSecurity.services.converters;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import com.example.testSecurity.dto.JuegoDto;
import com.example.testSecurity.entities.Juego;

@Service
public class JuegoDtoJuegoConverter implements Converter<JuegoDto, Juego>{

	@Autowired
	CompañiaDtoCompañiaConverter compConverter;
	
	@Override
	public Juego convert(JuegoDto source) {
		Juego juego = new Juego();
		juego.setTitulo(source.getTitulo());
		juego.setFechaLanzamiento(source.getFechaLanzamiento());
		juego.setCategoria(source.getCategoria());
		juego.setPegi(source.getPegi());
		return juego;
	}
	
	public JuegoDto inverseConvert(Juego source) {
		JuegoDto juego = new JuegoDto();
		juego.setId(source.getId());
		juego.setTitulo(source.getTitulo());
		juego.setFechaLanzamiento(source.getFechaLanzamiento());
		juego.setCategoria(source.getCategoria());
		juego.setPegi(source.getPegi());
		juego.setCompanias(compConverter.inverseConvertList(source.getListaCompañia()));
		return juego;
	}
	
	public List<JuegoDto>inverseConvertList(List<Juego>source){
		List<JuegoDto>salida = new ArrayList<>();
		for (Juego juego : source) {
			salida.add(inverseConvert(juego));
		}
		return salida;
	}

}
