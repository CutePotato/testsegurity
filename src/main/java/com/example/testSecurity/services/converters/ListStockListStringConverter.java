package com.example.testSecurity.services.converters;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import com.example.testSecurity.entities.Stock;
@Service
public class ListStockListStringConverter implements Converter<List<Stock>, List<String>>{

	@Override
	public List<String> convert(List<Stock> source) {
		List<String>salida = new ArrayList<>();
		for (Stock stock : source) {
			salida.add(stock.toString());
		}
		return salida;
	}

}
