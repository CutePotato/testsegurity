package com.example.testSecurity.services.converters;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import com.example.testSecurity.dto.TiendaDto;
import com.example.testSecurity.entities.Tienda;
@Service
public class TiendaDtoTiendaConverter implements Converter<TiendaDto, Tienda>{

	@Override
	public Tienda convert(TiendaDto source) {
		Tienda tienda = new Tienda();
		tienda.setNombre(source.getNombre());
		tienda.setDireccion(source.getDireccion());
		return tienda;
	}
	
	public TiendaDto inverseConvert(Tienda source) {
		TiendaDto tienda = new TiendaDto();
		tienda.setId(source.getId());
		tienda.setNombre(source.getNombre());
		tienda.setDireccion(source.getDireccion());
		return tienda;
	}
	
	public List<TiendaDto>inverseConvertList(List<Tienda>source){
		List<TiendaDto>salida = new ArrayList<>();
		for(Tienda t: source) {
			salida.add(inverseConvert(t));
		}
		return salida;
	}

}
