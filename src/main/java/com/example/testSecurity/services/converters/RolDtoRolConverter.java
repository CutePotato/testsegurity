package com.example.testSecurity.services.converters;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import com.example.testSecurity.dto.RolDto;
import com.example.testSecurity.entities.Rol;
@Service
public class RolDtoRolConverter implements Converter<RolDto, Rol>{

	@Override
	public Rol convert(RolDto source) {
		Rol rol = new Rol();
		rol.setRol(source.getRol());
		return rol;
	}
	
	public RolDto inverseConvert(Rol source) {
		RolDto rol = new RolDto();
		rol.setId(source.getId());
		rol.setRol(source.getRol());
		return rol;
	}
	
	public List<RolDto>inverseConvertList(List<Rol>source){
		List<RolDto>salida = new ArrayList<>();
		for (Rol rol : source) {
			salida.add(inverseConvert(rol));
		}
		return salida;
	}
	
}
