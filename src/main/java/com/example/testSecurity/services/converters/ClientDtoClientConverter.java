package com.example.testSecurity.services.converters;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.testSecurity.dto.ClienteDto;
import com.example.testSecurity.entities.Cliente;
import com.example.testSecurity.repositories.RolesRepository;
@Service
public class ClientDtoClientConverter implements Converter<ClienteDto, Cliente>{

	@Autowired
	RolesRepository rolRepository;
	@Autowired
	BCryptPasswordEncoder encoder;
	
	@Override
	public Cliente convert(ClienteDto source) {
		Cliente c = new Cliente();
		c.setNombre(source.getNombre());
		c.setDocumento(source.getDocumento());
		c.setCorreo(source.getCorreo());
		c.setFechaNacimiento(source.getFechaNacimiento());
		c.setUsername(source.getUsername());
		c.setPassword(encoder.encode(source.getPassword()));
		return c;
	}
	
	public ClienteDto inverseConvert(Cliente source) {
		ClienteDto c = new ClienteDto();
		c.setId(source.getId());
		c.setNombre(source.getNombre());
		c.setDocumento(source.getDocumento());
		c.setCorreo(source.getCorreo());
		c.setFechaNacimiento(source.getFechaNacimiento());
		c.setUsername(source.getUsername());
		c.setPassword(encoder.encode(source.getPassword()));
		return c;
	}
	
	public List<ClienteDto> inverseConvertList(List<Cliente>source){
		List<ClienteDto>salida = new ArrayList<>();
		for (Cliente cliente : source) {
			salida.add(inverseConvert(cliente));
		}
		return salida;
	}

}