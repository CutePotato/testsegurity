package com.example.testSecurity.services.converters;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import com.example.testSecurity.dto.CompañiaDto;
import com.example.testSecurity.entities.Compañia;
@Service
public class CompañiaDtoCompañiaConverter implements Converter<CompañiaDto, Compañia>{

	@Override
	public Compañia convert(CompañiaDto source) {
		Compañia c = new Compañia();
		c.setNombre(source.getNombre());
		c.setCif(source.getCif());
		return c;
	}
	
	public CompañiaDto inverseConvert(Compañia source) {
		CompañiaDto c = new CompañiaDto();
		c.setId(source.getId());
		c.setNombre(source.getNombre());
		c.setCif(source.getCif());
		return c;
	}
	
	public List<CompañiaDto> inverseConvertList(List<Compañia>source){
		List<CompañiaDto>salida = new ArrayList<>();
		for (Compañia compañia : source) {
			salida.add(inverseConvert(compañia));
		}
		return salida;
	}

}
