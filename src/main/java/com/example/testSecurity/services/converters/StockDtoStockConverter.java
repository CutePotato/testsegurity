package com.example.testSecurity.services.converters;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import com.example.testSecurity.dto.StockDto;
import com.example.testSecurity.entities.Stock;
@Service
public class StockDtoStockConverter implements Converter<StockDto, Stock>{

	@Override
	public Stock convert(StockDto source) {
		Stock stock = new Stock();
		stock.setEstado(source.getEstado());
		stock.setReference(source.getReference());
		return stock;
	}
	
	public StockDto inverseConvert(Stock source) {
		StockDto stock = new StockDto();
		stock.setId(source.getId());
		stock.setEstado(source.getEstado());
		stock.setReference(source.getReference());
		return stock;
	}
	
	public List<StockDto>inverseConvertList(List<Stock>source){
		List<StockDto>salida = new ArrayList<>();
		for (Stock stock : source) {
			salida.add(inverseConvert(stock));
		}
		return salida;
	}

}
