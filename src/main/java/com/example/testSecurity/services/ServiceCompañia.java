package com.example.testSecurity.services;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.testSecurity.dto.CompañiaDto;
import com.example.testSecurity.dto.JuegoDto;
import com.example.testSecurity.entities.Compañia;
import com.example.testSecurity.entities.Juego;
import com.example.testSecurity.interfaces.ServicesCompañia;
import com.example.testSecurity.repositories.CompañiasRepository;
import com.example.testSecurity.repositories.JuegosRepository;
import com.example.testSecurity.services.converters.CompañiaDtoCompañiaConverter;
@Service
public class ServiceCompañia implements ServicesCompañia{
	
	@Autowired
	CompañiasRepository compañiaRepository;
	@Autowired
	JuegosRepository juegoRepository;
	@Autowired
	CompañiaDtoCompañiaConverter converter;

	@Override
	public CompañiaDto createCompañia(@NotNull CompañiaDto comp) {
		Compañia c = converter.convert(comp);
		Compañia compSaved = compañiaRepository.save(c);
		return converter.inverseConvert(compSaved);
	}
	
	public List<CompañiaDto> readAllCompañias(){
		return converter.inverseConvertList(compañiaRepository.findAll());
	}

	@Override
	public CompañiaDto updateCompañia(Long id, CompañiaDto compañia) {
		Compañia c = compañiaRepository.findById(id).get();
		c.setNombre(compañia.getNombre());
		c.setCif(compañia.getCif());
		Compañia compSaved = compañiaRepository.save(c);
		return converter.inverseConvert(compSaved);
		
	}

	@Override
	public CompañiaDto deleteCompañia(Long id) {
		Compañia c = compañiaRepository.findById(id).get();
		CompañiaDto salida = converter.inverseConvert(c); 
		compañiaRepository.delete(c);
		return salida;
	}
	
	public void addJuego(int cif, JuegoDto juego) {
		Juego j = juegoRepository.findByTitulo(juego.getTitulo());
		Compañia compañia = compañiaRepository.findByCif(cif);
		j.getListaCompañia().add(compañia);
		juegoRepository.save(j);
		compañia.getListaJuego().add(j);
		compañiaRepository.save(compañia);
	}

	public List<Juego>readAllJuegos(int cif){
		Compañia compañia = compañiaRepository.findByCif(cif);
		return compañia.getListaJuego();
	}
	
	public void updateJuego(int cif, JuegoDto juego) {
		Juego j = juegoRepository.findByTitulo(juego.getTitulo());
		Compañia compañia = compañiaRepository.findByCif(cif);
		if(compañia.getListaJuego().contains(j)) {
			j.setTitulo(juego.getTitulo());
			j.setFechaLanzamiento(juego.getFechaLanzamiento());
			j.setCategoria(juego.getCategoria());
			j.setPegi(juego.getPegi());
			juegoRepository.save(j);
			compañiaRepository.save(compañia);
		}
	}
	
	public void deleteJuego(int cif, JuegoDto juego) {
		Juego j = juegoRepository.findByTitulo(juego.getTitulo());
		Compañia compañia = compañiaRepository.findByCif(cif);
		if(compañia.getListaJuego().contains(j) && j.getListaCompañia().contains(compañia)) {
			compañia.getListaJuego().remove(j);
			j.getListaCompañia().remove(compañia);
			juegoRepository.save(j);
			compañiaRepository.save(compañia);
		}
	}
}
