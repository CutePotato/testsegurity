package com.example.testSecurity.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.testSecurity.dto.StockDto;
import com.example.testSecurity.dto.TiendaDto;
import com.example.testSecurity.entities.Stock;
import com.example.testSecurity.entities.Tienda;
import com.example.testSecurity.interfaces.ServicesTienda;
import com.example.testSecurity.repositories.StocksRepository;
import com.example.testSecurity.repositories.TiendasRepository;
import com.example.testSecurity.services.converters.TiendaDtoTiendaConverter;
@Service
public class ServiceTienda implements ServicesTienda{
	
	@Autowired
	StocksRepository stockRepository;
	@Autowired
	TiendasRepository tiendaRepository;
	@Autowired
	TiendaDtoTiendaConverter converter;

	public List<TiendaDto>readAllTiendas(){
		List<Tienda>listaTiendas = tiendaRepository.findAll();
		return converter.inverseConvertList(listaTiendas);
	}
	
	@Override
	public TiendaDto createTienda(TiendaDto tienda) {
		Tienda t = converter.convert(tienda);
		Tienda tiendaSaved = tiendaRepository.save(t);
		return converter.inverseConvert(tiendaSaved);
	}

	@Override
	public TiendaDto updateTienda(Long id, TiendaDto tienda) {
		Tienda t = tiendaRepository.findById(id).get();
		t.setDireccion(tienda.getDireccion());
		t.setNombre(tienda.getNombre());
		Tienda tiendaSaved = tiendaRepository.save(t);
		return converter.inverseConvert(tiendaSaved);
	}

	@Override
	public TiendaDto deleteTienda(Long id) {
		Tienda t = tiendaRepository.findById(id).get();
		TiendaDto salida = converter.inverseConvert(t);
		tiendaRepository.delete(t);
		return salida;
	}
	
	public List<Stock>readAllStock(String nombre){
		Tienda tienda = tiendaRepository.findByNombre(nombre);
		return tienda.getListaStock();
	}
	
	public void addStock(String nombreTienda, StockDto stock) {
		Stock s = stockRepository.findByReference(stock.getReference());
		Tienda t = tiendaRepository.findByNombre(nombreTienda);
		s.setTienda(t);
		stockRepository.save(s);
		t.getListaStock().add(s);
		tiendaRepository.save(t);
	}
	
	public void updateStock(String nombre, StockDto stock) {
		Tienda t = tiendaRepository.findByNombre(nombre);
		Stock s = stockRepository.findByReference(stock.getReference());
		if(t.getListaStock().contains(s)) {
			s.setEstado(stock.getEstado());
			stockRepository.save(s);
		}
	}
	
	public void deleteStock(String nombre, StockDto stock) {
		Tienda t = tiendaRepository.findByNombre(nombre);
		Stock s = stockRepository.findByReference(stock.getReference());
		if(t.getListaStock().contains(s)) {
			s.setTienda(null);
			stockRepository.save(s);
			t.getListaStock().remove(s);
			tiendaRepository.save(t);
		}
	}
}
