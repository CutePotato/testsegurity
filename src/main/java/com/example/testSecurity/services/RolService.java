package com.example.testSecurity.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.testSecurity.dto.ClienteDto;
import com.example.testSecurity.dto.RolDto;
import com.example.testSecurity.entities.Cliente;
import com.example.testSecurity.entities.Rol;
import com.example.testSecurity.enums.RolCliente;
import com.example.testSecurity.repositories.ClientesRepository;
import com.example.testSecurity.repositories.RolesRepository;
import com.example.testSecurity.services.converters.RolDtoRolConverter;

@Service
public class RolService {
	@Autowired
	RolDtoRolConverter converter;
	@Autowired
	RolesRepository rolesRepository;
	@Autowired
	ClientesRepository clientesRepository;
	
	public RolDto createRol(RolDto rolDto) {
		Rol rol = converter.convert(rolDto);
		Rol r = rolesRepository.save(rol);
		return converter.inverseConvert(r);
	}
	
	public RolDto readRol(RolCliente rol){
		return converter.inverseConvert(rolesRepository.findByRol(rol));
	}
	
	public List<RolDto>readAllRol(){
		return converter.inverseConvertList(rolesRepository.findAll());
	}
	
	public RolDto deleteRol(Long id) {
		Rol r = rolesRepository.findById(id).get();
		RolDto salida = converter.inverseConvert(r);
		rolesRepository.delete(r);
		return salida;
	}
	
	public Boolean addCliente(RolCliente rolCliente, ClienteDto clienteDto) {
		Rol rol = rolesRepository.findByRol(rolCliente);
		Cliente cliente = clientesRepository.findByUsername(clienteDto.getUsername());
		if(rol.getClientes().contains(cliente)) {
			return false;
		}
		rol.getClientes().add(cliente);
		rolesRepository.save(rol);
		if(cliente.getRoles().contains(rol)) {
			return false;
		}
		Boolean salida = cliente.getRoles().add(rol);
		clientesRepository.save(cliente);
		return salida;
	}
	
	public List<Cliente>readClientes(RolCliente rolCliente){
		return rolesRepository.findByRol(rolCliente).getClientes();
	}
	
	public Boolean deleteCliente(RolCliente rolCliente, ClienteDto clienteDto) {
		Rol rol = rolesRepository.findByRol(rolCliente);
		Cliente cliente = clientesRepository.findByUsername(clienteDto.getUsername());
		if(!rol.getClientes().contains(cliente)) {
			return false;
		}
		Boolean salida = rol.getClientes().remove(cliente);
		rolesRepository.save(rol);
		return salida;
	}
}
