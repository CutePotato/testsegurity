package com.example.testSecurity.services;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.testSecurity.dto.ClienteDto;
import com.example.testSecurity.dto.StockDto;
import com.example.testSecurity.entities.Cliente;
import com.example.testSecurity.entities.Juego;
import com.example.testSecurity.entities.Rol;
import com.example.testSecurity.entities.Stock;
import com.example.testSecurity.enums.EstadoStock;
import com.example.testSecurity.enums.RolCliente;
import com.example.testSecurity.interfaces.ServicesCliente;
import com.example.testSecurity.repositories.ClientesRepository;
import com.example.testSecurity.repositories.RolesRepository;
import com.example.testSecurity.repositories.StocksRepository;
import com.example.testSecurity.services.converters.ClientDtoClientConverter;


@Service
public class ServiceCliente implements ServicesCliente, UserDetailsService{

	@Autowired
	ClientDtoClientConverter converter;
	@Autowired
	ClientesRepository clienteRepository;
	@Autowired
	StocksRepository stockRepository;
	@Autowired
	RolesRepository rolesRepository;

	@Override
	public ClienteDto createClient(@NotNull ClienteDto cliente) {
		Cliente c = converter.convert(cliente);
		Cliente clienteSaved = clienteRepository.save(c);
		return converter.inverseConvert(clienteSaved);
		
	}

	public ClienteDto readClient(Long id) {
		Cliente c =  clienteRepository.findById(id).get();
		ClienteDto cliente = converter.inverseConvert(c);
		return cliente;
	}
	
	public List<ClienteDto> readAllClient() {
		return converter.inverseConvertList(clienteRepository.findAll());
	}

	@Override
	public ClienteDto updateClient(Long id, ClienteDto cliente) {
		Cliente c = clienteRepository.findById(id).get();
		c.setNombre(cliente.getNombre());
		c.setCorreo(cliente.getCorreo());
		c.setFechaNacimiento(cliente.getFechaNacimiento());
		Cliente clienteSaved = clienteRepository.save(c);
		return converter.inverseConvert(clienteSaved);
	}

	@Override
	public ClienteDto deleteClient(Long id) {
		Cliente c = clienteRepository.findById(id).get();
		ClienteDto salida = converter.inverseConvert(c);
		clienteRepository.delete(c);
		return salida;
	}
	
	public Boolean addStock(@NotNull String username, @NotNull int reference) {
		Cliente c = clienteRepository.findByUsername(username);
		Stock s = stockRepository.findByReference(reference);
		if(c.getListaStock().contains(s)) {
			return false;
		}
		Boolean isAdmin=false;
		for (Rol rol : c.getRoles()) {
			if(rol.getRol().equals(RolCliente.ADMIN)) {
				isAdmin=true;
			}
		}
		if(!isAdmin) {
			return false;
		}
		Boolean isRented = s.getEstado().equals(EstadoStock.ALQUILADO);
		Boolean hasAgeToBuy = hasAge(c, s);
		if(!hasRentedStock(c) && hasAgeToBuy) {
			s.setCliente(c);
			stockRepository.save(s);
			c.getListaStock().add(s);
			clienteRepository.save(c);
			return true;
		}
		if(hasRentedStock(c) && isRented && hasAgeToBuy) {
			return false;
		}
		if(hasRentedStock(c) && !isRented && !hasAgeToBuy) {
			return false;
		}
		s.setCliente(c);
		stockRepository.save(s);
		c.getListaStock().add(s);
		clienteRepository.save(c);
		return true;
	}
	
	private Boolean hasAge(@NotNull Cliente cliente, @NotNull Stock stock) {
		Juego juego = stock.getJuego();
		Date edad = cliente.getFechaNacimiento();
		Date ahora = new Date();
		long diff = edad.getTime()-ahora.getTime();
		long añoMili = 525600*60*1000;
		long edadCliente = diff/añoMili;
		long pegi = juego.getPegi();
		if(edadCliente>pegi) {
			return true;
		}
		return false;
	}
	
	private Boolean hasRentedStock(@NotNull Cliente cliente) {
		for (Stock stock : cliente.getListaStock()) {
			if(stock.getEstado().equals(EstadoStock.ALQUILADO))
				return true;
		}
		return false;
	}
	
	public List<Stock> readAllStock(@NotNull String username){
		Cliente cliente = clienteRepository.findByUsername(username);
		return cliente.getListaStock();
	}
	
	public Boolean updateStock(@NotNull String username, @NotNull StockDto source) {
		Cliente cliente = clienteRepository.findByUsername(username);
		Stock stock = stockRepository.findByReference(source.getReference());
		if(!cliente.getListaStock().contains(stock)) {
			return false;
		}
		List<Rol>listaRoles = cliente.getRoles();
		for (Rol rol : listaRoles) {
			if(rol.getRol().equals(RolCliente.ADMIN)) {
				stock.setEstado(source.getEstado());
				stockRepository.save(stock);
				return true;
			}
		}
		return false;
	}
	
	public Boolean deleteStock(@NotNull String username, @NotNull StockDto source) {
		Cliente cliente = clienteRepository.findByUsername(username);
		Stock stock = stockRepository.findByReference(source.getReference());
		if(!cliente.getListaStock().contains(stock)) {
			return false;
		}
		List<Rol>listaRoles = cliente.getRoles();
		for (Rol rol : listaRoles) {
			if(rol.getRol().equals(RolCliente.ADMIN)) {
				stock.setCliente(null);
				stockRepository.save(stock);
				cliente.getListaStock().remove(stock);
				clienteRepository.save(cliente);
				return true;
			}
		}
		return false;
	}
	
	public Boolean addRol(String username, RolCliente rolCliente) {
		Rol rol = rolesRepository.findByRol(rolCliente);
		Cliente cliente = clienteRepository.findByUsername(username);
		if(cliente.getRoles().contains(rol)) {
			return false;
		}
		cliente.getRoles().add(rol);
		clienteRepository.save(cliente);
		if(rol.getClientes().contains(cliente)) {
			return false;
		}
		rol.getClientes().add(cliente);
		rolesRepository.save(rol);
		return true;
		
	}
	public List<Rol>readRoles(String username){
		return clienteRepository.findByUsername(username).getRoles();
	}
	public Boolean deleteRol(String username, RolCliente rolCliente) {
		Rol rol = rolesRepository.findByRol(rolCliente);
		Cliente cliente = clienteRepository.findByUsername(username);
		if(!cliente.getRoles().contains(rol)) {
			return false;
		}
		Boolean salida = cliente.getRoles().remove(rol);
		clienteRepository.save(cliente);
		return salida;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Cliente c = clienteRepository.findByUsername(username);
		return new User(c.getUsername(), c.getPassword(), c.getRoles());
	}
}
