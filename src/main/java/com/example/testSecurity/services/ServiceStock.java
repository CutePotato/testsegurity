package com.example.testSecurity.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.testSecurity.dto.StockDto;
import com.example.testSecurity.entities.Stock;
import com.example.testSecurity.interfaces.ServicesStock;
import com.example.testSecurity.repositories.StocksRepository;
import com.example.testSecurity.services.converters.StockDtoStockConverter;
@Service
public class ServiceStock implements ServicesStock{
	@Autowired
	StocksRepository stockRepository;
	@Autowired
	StockDtoStockConverter converter;

	@Override
	public StockDto createStock(StockDto stock) {
		Stock s = converter.convert(stock);
		Stock stockSaved = stockRepository.save(s);
		return converter.inverseConvert(stockSaved);
	}
	
	public List<StockDto>readAllStocks(){
		return converter.inverseConvertList(stockRepository.findAll());
	}

	@Override
	public StockDto updateStock(Long id, StockDto stock) {
		Stock s = stockRepository.findById(id).get();
		s.setEstado(stock.getEstado());
		s.setReference(stock.getReference());
		Stock stockSaved = stockRepository.save(s);
		return converter.inverseConvert(stockSaved);
	}

	@Override
	public StockDto deleteStock(Long id) {
		Stock s = stockRepository.findById(id).get();
		StockDto salida = converter.inverseConvert(s);
		stockRepository.delete(s);
		return salida;
	}
}
