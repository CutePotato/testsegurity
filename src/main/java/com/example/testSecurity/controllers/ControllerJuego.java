package com.example.testSecurity.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.testSecurity.dto.CompañiaDto;
import com.example.testSecurity.dto.JuegoDto;
import com.example.testSecurity.dto.StockDto;
import com.example.testSecurity.errors.operations.JuegoException;
import com.example.testSecurity.interfaces.ServicesJuego;
import com.example.testSecurity.services.ServiceJuego;
@RestController
@RequestMapping("/juego")
public class ControllerJuego implements ServicesJuego{
	@Autowired
	ServiceJuego serviceJuego;

	@PostMapping("/create")
	public ResponseEntity<JuegoDto> createJuegoEntity(@RequestBody JuegoDto juego) {
		try {
			return new ResponseEntity<JuegoDto>(createJuego(juego), HttpStatus.OK);
		} catch (Exception e) {
			throw new JuegoException("Fallo al crear el juego: ");
		}
	}
	
	@Override
	public JuegoDto createJuego(@RequestBody JuegoDto juego) {
		try {
			return serviceJuego.createJuego(juego);
		} catch (Exception e) {
			throw new JuegoException("Fallo al crear el juego: ");
		}
	}
	
	@GetMapping("/read-all")
	public ResponseEntity<List<JuegoDto>>readJuegos(){
		List<JuegoDto>lista = serviceJuego.readAllJuego();
		return new ResponseEntity<List<JuegoDto>>(lista, HttpStatus.OK);
	}

	@PutMapping("/{id}/update")
	public ResponseEntity<JuegoDto> updateJuegoEntity(@PathVariable Long id, @RequestBody JuegoDto juego) {
		try {
			return new ResponseEntity<JuegoDto>(updateJuego(id, juego), HttpStatus.OK);
		} catch (Exception e) {
			throw new JuegoException("Fallo al actualizar el juego: ");
		}
	}
	
	@Override
	public JuegoDto updateJuego(@PathVariable Long id, @RequestBody JuegoDto juego) {
		try {
			return serviceJuego.updateJuego(id, juego);
		} catch (Exception e) {
			throw new JuegoException("Fallo al actualizar el juego: ");
		}
	}

	@DeleteMapping("/{id}/delete")
	public ResponseEntity<JuegoDto> deleteJuegoEntity(@PathVariable Long id) {
		try {
			return new ResponseEntity<JuegoDto>(deleteJuego(id), HttpStatus.OK);
		} catch (Exception e) {
			throw new JuegoException("Fallo al borrar el juego: ");
		}
	}
	
	@Override
	public JuegoDto deleteJuego(@PathVariable Long id) {
		try {
			return serviceJuego.deleteJuego(id);
		} catch (Exception e) {
			throw new JuegoException("Fallo al borrar el juego: ");
		}
	}
	
	@PostMapping("/add-stock")
	public void addStock(@RequestParam String titulo, @RequestBody StockDto stock) {
		try {
			serviceJuego.addStock(titulo, stock);
		} catch (Exception e) {
			throw new JuegoException("Fallo al añadir el stock al juego: ");
		}
	}
	
	@GetMapping("/read-stock")
	public ResponseEntity<List<String>>readStock(@RequestParam String titulo){
		try {
			List<String>listaStock = serviceJuego.readAllStocks(titulo);
			return new ResponseEntity<List<String>>(listaStock, HttpStatus.OK);
		} catch (Exception e) {
			throw new JuegoException("Fallo al leer el stock del juego: ");
		}
	}

	@PostMapping("/update-stock")
	public void updateStock(@RequestParam String titulo, @RequestBody StockDto stock) {
		try {
			serviceJuego.updateStock(titulo, stock);
		} catch (Exception e) {
			throw new JuegoException("Fallo al actualizar el stock del juego: ");
		}
	}
	
	@DeleteMapping("/delete-stock")
	public void deleteStock(@RequestParam String titulo, @RequestBody StockDto stock) {
		try {
			serviceJuego.deleteStock(titulo, stock);
		} catch (Exception e) {
			throw new JuegoException("Fallo al borrar el stock del juego: ");
		}
	}
	
	@PostMapping("/add-comp")
	public void addCompañia(@RequestParam String titulo, @RequestBody CompañiaDto compañia) {
		try {
			serviceJuego.addCompañia(titulo, compañia);
		} catch (Exception e) {
			throw new JuegoException("Fallo al añadir la compañia al juego: ");
		}
	}
	
	@GetMapping("/read-comp")
	public ResponseEntity<List<String>>readAllCompañias(@RequestParam String titulo){
		try {
			List<String>listaCompañia = serviceJuego.readAllCompañias(titulo);
			return new ResponseEntity<List<String>>(listaCompañia, HttpStatus.OK);
		} catch (Exception e) {
			throw new JuegoException("Fallo al leer la compañia del juego: ");
		}
	}
	
	@PostMapping("/update-comp")
	public void updateCompañia(@RequestParam String titulo, @RequestBody CompañiaDto compañia) {
		try {
			serviceJuego.updateCompañia(titulo, compañia);
		} catch (Exception e) {
			throw new JuegoException("Fallo al actualizar la compañia del juego: ");
		}
	}
	
	@DeleteMapping("/delete-comp")
	public void deleteCompañia(@RequestParam String titulo, @RequestBody CompañiaDto compañia) {
		try {
			serviceJuego.deleteCompañia(titulo, compañia);
		} catch (Exception e) {
			throw new JuegoException("Fallo al borrar la compañia al juego: ");
		}
	}
}