package com.example.testSecurity.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.testSecurity.dto.ClienteDto;
import com.example.testSecurity.dto.RolDto;
import com.example.testSecurity.entities.Cliente;
import com.example.testSecurity.enums.RolCliente;
import com.example.testSecurity.services.RolService;

@RestController
@RequestMapping("/rol")
public class RolController {
	@Autowired
	RolService rolService;
	
	@PostMapping("/create")
	public ResponseEntity<RolDto>createRolEntity(@RequestBody RolDto rolDto){
		return new ResponseEntity<RolDto>(createRol(rolDto), HttpStatus.OK);
	}
	
	public RolDto createRol(@RequestBody RolDto rolDto) {
		return rolService.createRol(rolDto);
	}
	
	@GetMapping("/read")
	public ResponseEntity<RolDto> readRol(@RequestParam RolCliente rol) {
		return new ResponseEntity<RolDto>(rolService.readRol(rol), HttpStatus.OK);
	}
	
	@GetMapping("/read-all")
	public ResponseEntity<List<RolDto>>readAllRol(){
		return new ResponseEntity<List<RolDto>>(rolService.readAllRol(), HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}/delete")
	public ResponseEntity<RolDto> deleteRolEntity(@PathVariable Long id) {
		return new ResponseEntity<RolDto>(deleteRol(id), HttpStatus.OK);
	}
	
	private RolDto deleteRol(Long id) {
		return rolService.deleteRol(id);
	}
	
	@PostMapping("/add-client")
	public void addClientRol(@RequestParam RolCliente rol, @RequestBody ClienteDto clientDto) {
		rolService.addCliente(rol, clientDto);
	}
	
	@GetMapping("/read-client")
	public ResponseEntity<List<Cliente>> readClientRol(@RequestParam RolCliente rol) {
		List<Cliente>listaClientes = rolService.readClientes(rol);
		return new ResponseEntity<List<Cliente>>(listaClientes, HttpStatus.OK);
	}
	
	@DeleteMapping("/delete-client")
	public void deleteClientRol(@RequestParam RolCliente rol, @RequestBody ClienteDto cliente) {
		rolService.deleteCliente(rol, cliente);
	}
}
