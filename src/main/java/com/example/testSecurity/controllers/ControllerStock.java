package com.example.testSecurity.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.testSecurity.dto.StockDto;
import com.example.testSecurity.errors.operations.StockException;
import com.example.testSecurity.interfaces.ServicesStock;
import com.example.testSecurity.services.ServiceStock;

@RestController
@RequestMapping("/stock")
public class ControllerStock implements ServicesStock{

	@Autowired
	ServiceStock serviceStock;
	
	@PostMapping("/create")
	public ResponseEntity<StockDto>createStockEntity(@RequestBody StockDto stock){
		return new ResponseEntity<StockDto>(serviceStock.createStock(stock), HttpStatus.OK);
	}
	
	@Override
	public StockDto createStock(StockDto stock) {
		try {
			return serviceStock.createStock(stock);
		} catch (Exception e) {
			throw new StockException("Fallo al crear el stock");
		}
	}
	
	@GetMapping("/read-all")
	public ResponseEntity<List<StockDto>>readAllStock(){
		return new ResponseEntity<List<StockDto>>(serviceStock.readAllStocks(), HttpStatus.OK);
	}

	@PutMapping("/{id}/update")
	public ResponseEntity<StockDto>updateStockEntity(@PathVariable Long id, @RequestBody StockDto stock){
		return new ResponseEntity<StockDto>(serviceStock.updateStock(id, stock), HttpStatus.OK);
	}
	
	@Override
	public StockDto updateStock(Long id, StockDto stock) {
		try {
			return serviceStock.updateStock(id, stock);
		} catch (Exception e) {
			throw new StockException("Fallo al actualizar el stock");
		}
	}

	@DeleteMapping("/{id}/delete")
	public ResponseEntity<StockDto>deleteStockEntity(@PathVariable Long id){
		return new ResponseEntity<StockDto>(serviceStock.deleteStock(id), HttpStatus.OK);
	}
	
	@Override
	public StockDto deleteStock(Long id) {
		try {
			return serviceStock.deleteStock(id);
		} catch (Exception e) {
			throw new StockException("Fallo al borrar el stock");
		}
	}
}
