package com.example.testSecurity.controllers;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.testSecurity.dto.ClienteDto;
import com.example.testSecurity.dto.StockDto;
import com.example.testSecurity.entities.Rol;
import com.example.testSecurity.entities.Stock;
import com.example.testSecurity.enums.RolCliente;
import com.example.testSecurity.errors.operations.ClientException;
import com.example.testSecurity.interfaces.ServicesCliente;
import com.example.testSecurity.services.ServiceCliente;

@RestController
@RequestMapping("/client")
public class ControllerCliente implements ServicesCliente{
	@Autowired
	ServiceCliente serviceCliente;

	@PostMapping("/create")
	public ResponseEntity<ClienteDto>createClienteEntity(@RequestBody ClienteDto cliente){
		return new ResponseEntity<ClienteDto>(createClient(cliente), HttpStatus.OK);
	}
	
	@Override
	public ClienteDto createClient(ClienteDto cliente) {
		try {
			return serviceCliente.createClient(cliente);
		} catch (Exception e) {
			throw new ClientException("Fallo al crear el cliente");
		}
	}

	@GetMapping("/read")
	public ResponseEntity<ClienteDto> readClientEntity(@PathVariable Long id) {
		return new ResponseEntity<ClienteDto>(readClient(id), HttpStatus.OK);
	}
	
	public ClienteDto readClient(Long id) {
		return serviceCliente.readClient(id);
	}
	
	@GetMapping("/read-all")
	public ResponseEntity<List<ClienteDto>>listaClientes(){
		List<ClienteDto> lista = serviceCliente.readAllClient();
		return new ResponseEntity<List<ClienteDto>>(lista ,HttpStatus.OK);
	}

	@PostMapping("/{id}/update")
	public ResponseEntity<ClienteDto> updateClientEntity(@PathVariable Long id, @Valid @RequestBody ClienteDto cliente) {
		return new ResponseEntity<ClienteDto>(updateClient(id, cliente), HttpStatus.OK);
	}
	
	@Override
	public ClienteDto updateClient(Long id, ClienteDto cliente) {
		try {
			return serviceCliente.updateClient(id, cliente);
		} catch (Exception e) {
			throw new ClientException("Fallo al actualizar el cliente");
		}
	}

	@DeleteMapping("/{id}/delete")
	public ResponseEntity<ClienteDto> deleteClientEntity(@PathVariable Long id) {
		return new ResponseEntity<ClienteDto>(deleteClient(id), HttpStatus.OK);
	}
	
	@Override
	public ClienteDto deleteClient(Long id) {
		try {
			return serviceCliente.deleteClient(id);
		} catch (Exception e) {
			throw new ClientException("Fallo al borrar el cliente");
		}
	}
	
	@PostMapping("/add-stock")
	public Boolean addStock(@NotNull @Size(min=3, max=12) @RequestParam String documento,@NotNull @Min(3) @RequestParam int reference) {
		try {
			return serviceCliente.addStock(documento, reference);
		} catch (Exception e) {
			throw new ClientException("Fallo al añadir stock al cliente");
		}
	}
	
	@GetMapping("/read-stock")
	public ResponseEntity<List<Stock>>readStock(@RequestParam String documento){
		try {
			List<Stock>listaStock = serviceCliente.readAllStock(documento);
			return new ResponseEntity<List<Stock>>(listaStock, HttpStatus.OK);
		} catch (Exception e) {
			throw new ClientException("Fallo al leer el stock del cliente");
		}
	}
	
	@PostMapping("/update-stock")
	public Boolean updateStock(@RequestParam String documento, @RequestBody StockDto stock) {
		try {
			return serviceCliente.updateStock(documento, stock);
		} catch (Exception e) {
			throw new ClientException("Fallo al actualizar el stock del cliente");
		}
	}
	
	@DeleteMapping("/delete-stock")
	public Boolean deleteStock(@RequestParam String documento, @RequestBody StockDto stock) {
		try {
			return serviceCliente.deleteStock(documento, stock);
		} catch (Exception e) {
			throw new ClientException("Fallo al borrar stock al cliente");
		}
	}
	
	@PostMapping("/add-rol")
	public Boolean addRolClient(String documento, RolCliente rol) {
		try {
			return serviceCliente.addRol(documento, rol);
		} catch (Exception e) {
			throw new ClientException("Fallo al añadir rol al cliente");
		}
	}
	
	@GetMapping("/read-rol")
	public ResponseEntity<List<Rol>> readRolClient(String username) {
		try {
			return new ResponseEntity<List<Rol>>(serviceCliente.readRoles(username), HttpStatus.OK);
		} catch (Exception e) {
			throw new ClientException("Fallo al leer rol al cliente");
		}
	}
	
	@DeleteMapping("/delete-rol")
	public void deleteRolClient(String documento, RolCliente rol) {
		try {
			serviceCliente.deleteRol(documento, rol);
		} catch (Exception e) {
			throw new ClientException("Fallo al borrar rol al cliente");
		}
	}
}
