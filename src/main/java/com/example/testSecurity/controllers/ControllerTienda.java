package com.example.testSecurity.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.testSecurity.dto.StockDto;
import com.example.testSecurity.dto.TiendaDto;
import com.example.testSecurity.errors.operations.TiendaException;
import com.example.testSecurity.interfaces.ServicesTienda;
import com.example.testSecurity.services.ServiceTienda;
@RestController
@RequestMapping("/tienda")
public class ControllerTienda implements ServicesTienda{

	@Autowired
	ServiceTienda serviceTienda;
	
	@GetMapping("/read-all")
	public ResponseEntity<List<TiendaDto>>readAllTiendas(){
		return new ResponseEntity<List<TiendaDto>>(serviceTienda.readAllTiendas(), HttpStatus.OK);
	}
	
	@PostMapping("/create")
	public ResponseEntity<TiendaDto>createTiendaEntity(@RequestBody TiendaDto tienda){
		return new ResponseEntity<TiendaDto>(createTienda(tienda), HttpStatus.OK);
	}
	
	@Override
	public TiendaDto createTienda(TiendaDto tienda) {
		try {
			return serviceTienda.createTienda(tienda);
		} catch (Exception e) {
			throw new TiendaException("Fallo al crear la tienda");
		}
	}

	@PutMapping("/{id}/update")
	public ResponseEntity<TiendaDto>updateTiendaEntity(@PathVariable Long id, @RequestBody TiendaDto tienda){
		return new ResponseEntity<TiendaDto>(updateTienda(id, tienda), HttpStatus.OK);
	}
	
	@Override
	public TiendaDto updateTienda(Long id, TiendaDto tienda) {
		try {
			return serviceTienda.updateTienda(id, tienda);
		} catch (Exception e) {
			throw new TiendaException("Fallo al actualizar la tienda");
		}
	}

	@DeleteMapping("/{id}/delete")
	public ResponseEntity<TiendaDto>deleteTiendaEntity(@PathVariable Long id){
		return new ResponseEntity<TiendaDto>(deleteTienda(id), HttpStatus.OK);
	}
	
	@Override
	public TiendaDto deleteTienda(Long id) {
		try {
			return serviceTienda.deleteTienda(id);
		} catch (Exception e) {
			throw new TiendaException("Fallo al borrar la tienda");
		}
	}
	
	@PostMapping("/add-stock")
	public void addStock(@RequestParam String nombreTienda, @RequestBody StockDto stock) {
		try {
			serviceTienda.addStock(nombreTienda, stock);
		} catch (Exception e) {
			throw new TiendaException("Fallo al añadir stock a la tienda");
		}
	}
	
	@PostMapping("/update-stock")
	public void updateStock(@RequestParam String nombre, @RequestBody StockDto stock) {
		try {
			serviceTienda.updateStock(nombre, stock);
		} catch (Exception e) {
			throw new TiendaException("Fallo al actualizar el stock de la tienda");
		}
	}
	
	@DeleteMapping("/delete-stock")
	public void deleteStock(@RequestParam String nombre, @RequestBody StockDto stock) {
		try {
			serviceTienda.deleteStock(nombre, stock);
		} catch (Exception e) {
			throw new TiendaException("Fallo al borrar el stock de la tienda");
		}
	}

}
