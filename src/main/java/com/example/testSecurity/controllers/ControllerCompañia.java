package com.example.testSecurity.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.testSecurity.dto.CompañiaDto;
import com.example.testSecurity.dto.JuegoDto;
import com.example.testSecurity.entities.Juego;
import com.example.testSecurity.errors.operations.CompañiaException;
import com.example.testSecurity.interfaces.ServicesCompañia;
import com.example.testSecurity.services.ServiceCompañia;
@RestController
@RequestMapping("/comp")
public class ControllerCompañia implements ServicesCompañia{
	
	@Autowired
	ServiceCompañia serviceComp;
	
	@PostMapping("/create")
	public ResponseEntity<CompañiaDto>createCompañiaEntity(@RequestBody CompañiaDto comp){
		return new ResponseEntity<CompañiaDto>(createCompañia(comp), HttpStatus.OK);
	}
	
	@Override
	public CompañiaDto createCompañia(CompañiaDto comp) {
		try {
			return serviceComp.createCompañia(comp);
		} catch (Exception e) {
			throw new CompañiaException("Fallo al crear la compañia");
		}
	}

	@GetMapping("/read-all")
	public ResponseEntity<List<CompañiaDto>>readAllCompañias(){
		List<CompañiaDto>listaCompañias = serviceComp.readAllCompañias();
		return new ResponseEntity<List<CompañiaDto>>(listaCompañias, HttpStatus.OK);
	}

	@PostMapping("/{id}/update")
	public ResponseEntity<CompañiaDto>updateCompañiaEntity(@PathVariable Long id, @RequestBody CompañiaDto comp){
		return new ResponseEntity<CompañiaDto>(updateCompañia(id, comp), HttpStatus.OK);
	}
	
	@Override
	public CompañiaDto updateCompañia(Long id, CompañiaDto compañia) {
		try {
			return serviceComp.updateCompañia(id, compañia);
		} catch (Exception e) {
			throw new CompañiaException("Fallo al actualizar la compañia");
		}
	}

	@DeleteMapping("/{id}/delete")
	public ResponseEntity<CompañiaDto>updateCompañiaEntity(@PathVariable Long id){
		return new ResponseEntity<CompañiaDto>(deleteCompañia(id), HttpStatus.OK);
	}
	
	@Override
	public CompañiaDto deleteCompañia(Long id) {
		try {
			return serviceComp.deleteCompañia(id);
		} catch (Exception e) {
			throw new CompañiaException("Fallo al eliminar la compañia");
		}
	}

	@PostMapping("/add-juego")
	public void addJuego(@RequestParam int cif, @RequestBody JuegoDto juego) {
		try {
			serviceComp.addJuego(cif, juego);
		} catch (Exception e) {
			throw new CompañiaException("Fallo al añidir juego a la compañia");
		}
	}

	@GetMapping("/read-juego")
	public ResponseEntity<List<Juego>>readAllJuegos(@RequestParam int cif){
		try {
			List<Juego> juegos = serviceComp.readAllJuegos(cif);
			return new ResponseEntity<List<Juego>>(juegos, HttpStatus.OK);
		} catch (Exception e) {
			throw new CompañiaException("Fallo al leer juego a la compañia");
		}
	}
	
	@PostMapping("/update-juego")
	public void updateJuego(@RequestParam int cif, @RequestBody JuegoDto juego) {
		try {
			serviceComp.updateJuego(cif, juego);
		} catch (Exception e) {
			throw new CompañiaException("Fallo al actualizar el juego a la compañia");
		}
	}
	
	@DeleteMapping("/delete-juego")
	public void deleteJuego(int cif, JuegoDto juego) {
		try {
			serviceComp.deleteJuego(cif, juego);
		} catch (Exception e) {
			throw new CompañiaException("Fallo al borrar el juego a la compañia");
		}
	}
}
