package com.example.testSecurity;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import com.example.testSecurity.dto.*;
import com.example.testSecurity.entities.*;
import com.example.testSecurity.enums.*;
import com.example.testSecurity.repositories.*;
import com.example.testSecurity.services.converters.*;

@SpringBootTest
class TestSecurityApplicationTests {

	@Autowired
	ClientesRepository clienteRepository;
	@Autowired
	BCryptPasswordEncoder encoder;
	@Autowired
	ClientDtoClientConverter converter;
	@Autowired
	RolDtoRolConverter converterRol;
	@Autowired
	TiendaDtoTiendaConverter converterTienda;
	@Autowired
	TiendasRepository tiendaRepository;
	@Autowired
	JuegosRepository juegoRepository;
	@Autowired
	StockDtoStockConverter converterStock;
	@Autowired
	JuegoDtoJuegoConverter converterJuego;
	@Autowired
	StocksRepository stockRepository;
	@Autowired
	RolesRepository rolRepository;
	@Autowired
	CompañiaDtoCompañiaConverter converterCompañia;
	@Autowired
	CompañiasRepository compañiaRepository;
	
	private Logger log = LogManager.getLogger();
	/*
	@Test
	void crearCliente() {
		ClienteDto clienteDto = new ClienteDto();
		clienteDto.setNombre("Pepe");
		clienteDto.setDocumento("12345");
		clienteDto.setCorreo("pepe@hola.es");
		Date fechaNacimiento = new Date(2000,3,3);
		clienteDto.setFechaNacimiento(fechaNacimiento);
		clienteDto.setPassword("password");
		clienteDto.setUsername("pepito");
		Cliente cliente = converter.convert(clienteDto);
		clienteRepository.save(cliente);
	}*/
	/*
	@Test
	void addStockCliente() {
		Cliente cliente = clienteRepository.findByDocumento("12345");
		Stock stock = stockRepository.findByReference(12345);
		stock.setCliente(cliente);
		stockRepository.save(stock);
		cliente.getListaStock().add(stock);
		clienteRepository.save(cliente);
	}
	*/
	/*@Test
	@Transactional
	void addRolCliente() {
		Cliente cliente = clienteRepository.findByDocumento("12345");
		Rol rol = rolRepository.findByRol(RolCliente.USER);
		cliente.getRoles().add(rol);
		rol.getClientes().add(cliente);
		rolRepository.save(rol);
		clienteRepository.save(cliente);
	}
*/
}
